package com.ewansr.mx.dontires;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.models.LoginModel;
import com.ewansr.mx.dontires.request.LoginRequest;

import mehdi.sakout.fancybuttons.FancyButton;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends LoginRequest {

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    // UI references.
    private Context mContext;
    private EditText mEmailView;
    private EditText mPasswordView;
    private FancyButton mLoginButton;
    private View mProgressView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mContext = LoginActivity.this;
        mEmailView    = findViewById(R.id.edtUser);
        mPasswordView = findViewById(R.id.edtPassword);
        mLoginButton  = findViewById(R.id.btnLogin);
        mProgressView = findViewById(R.id.progress);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    login(mEmailView.getText().toString(), mPasswordView.getText().toString());
                }
            }
        });

    }

    private Boolean isValid(){
        Boolean valid = true;
        if(mEmailView.getText().toString().isEmpty()) {
            mEmailView.setError("Nombre de Usuario o Correo obligatorio");
            valid = false;
        }

        if(mPasswordView.getText().toString().isEmpty()) {
            mPasswordView.setError("Escribe la contraseña para " + mEmailView.getText().toString());
            valid = false;
        }
        return valid;
    }


    @Override
    public void onBeforeLoad() {mProgressView.setVisibility(View.VISIBLE); }

    @Override
    public void onReady(LoginModel mUsuario) {
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
        mProgressView.setVisibility(View.INVISIBLE);
        finish();
    }

    @Override
    public void onError(mException error) {
        final android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder( mContext );
        builder.setTitle( error.title );
        builder.setMessage( error.message );

        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        }).show();

        mProgressView.setVisibility(View.GONE);
    }
}

