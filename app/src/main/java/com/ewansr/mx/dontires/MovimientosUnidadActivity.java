package com.ewansr.mx.dontires;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.models.MovimientoModel;
import com.ewansr.mx.dontires.request.MovimientosRequest;

import java.lang.reflect.Array;
import java.util.ArrayList;

import mehdi.sakout.fancybuttons.FancyButton;

public class MovimientosUnidadActivity extends MovimientosRequest {
    private FancyButton pos1;
    private FancyButton pos2;
    private FancyButton pos3;
    private FancyButton pos4;
    private FancyButton pos5;
    private FancyButton pos6;
    private FancyButton pos7;
    private FancyButton pos8;
    private FancyButton pos9;
    private FancyButton pos10;
    private MovimientoModel mData;
    private TextView cUnidad;
    private ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimientos_unidad);

        //mData = (MovimientoModel) getIntent().getExtras().getSerializable("datos");

        String unidad = getIntent().getStringExtra("unidad");

        pos1 = findViewById(R.id.tvPos1);
        pos2 = findViewById(R.id.tvPos2);
        pos3 = findViewById(R.id.tvPos3);
        pos4 = findViewById(R.id.tvPos4);
        pos5 = findViewById(R.id.tvPos5);
        pos6 = findViewById(R.id.tvPos6);
        pos7 = findViewById(R.id.tvPos7);
        pos8 = findViewById(R.id.tvPos8);
        pos9 = findViewById(R.id.tvPos9);
        pos10 = findViewById(R.id.tvPos10);
        cUnidad = findViewById(R.id.cNumUnidad);
        btnBack = findViewById(R.id.btnBack);
        cUnidad.setText(unidad);

       // consultaUltimosMontajes(mData.getUnidad());
        consultaUltimosMontajes(unidad);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBeforeLoad() {

    }

    @Override
    public void onReady(ArrayList<MovimientoModel> mData) {
        for(int i= 0; i < mData.size(); i++){
            switch (mData.get(i).getIntPosicion()){
                case 1:
                    pos1.setText(mData.get(i).getNoLlanta());
                    break;
                case 2:
                    pos2.setText(mData.get(i).getNoLlanta());
                    break;
                case 3:
                    pos3.setText(mData.get(i).getNoLlanta());
                    break;
                case 4:
                    pos4.setText(mData.get(i).getNoLlanta());
                    break;
                case 5:
                    pos5.setText(mData.get(i).getNoLlanta());
                    break;
                case 6:
                    pos6.setText(mData.get(i).getNoLlanta());
                    break;
                case 7:
                    pos7.setText(mData.get(i).getNoLlanta());
                    break;
                case 8:
                    pos8.setText(mData.get(i).getNoLlanta());
                    break;
                case 9:
                    pos9.setText(mData.get(i).getNoLlanta());
                    break;
                case 10:
                    pos10.setText(mData.get(i).getNoLlanta());
                    break;

            }
        }


    }

    @Override
    public void onError(mException error) {

    }
}
