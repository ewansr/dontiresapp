package com.ewansr.mx.dontires.models;

import android.os.Bundle;

import java.io.Serializable;

public class MovimientoModel implements Serializable{
    Integer id_movimiento;
    String fecha_hora;
    String unidad;
    Integer posicion;
    Double medida;
    String diseno;
    String no_llanta;
    String tipo_llanta;
    Double milimitraje;
    String causa;
    String rango_dano;
    String aplica;
    String observaciones;
    String foto;
    Integer id_usuario;
    Integer clasificacion;
    String nombre_clasificacion;

    public MovimientoModel(Integer id_movimiento, String fecha_hora, String unidad, Integer posicion,
                           Double medida, String diseno, String no_llanta, String tipo_llanta,
                           Double milimitraje, String causa, String rango_dano, String aplica,
                           String observaciones, String foto, Integer id_usuario){
        this.id_movimiento=id_movimiento;
        this.fecha_hora=fecha_hora;
        this.unidad=unidad;
        this.posicion=posicion;
        this.medida=medida;
        this.diseno=diseno;
        this.no_llanta=no_llanta;
        this.tipo_llanta=tipo_llanta;
        this.milimitraje=milimitraje;
        this.causa=causa;
        this.rango_dano=rango_dano;
        this.aplica=aplica;
        this.observaciones=observaciones;
        this.foto=foto;
        this.id_usuario=id_usuario;

        this.clasificacion = -1;
        this.nombre_clasificacion = "_";
    }

    // Sobrecarga del Método
    public MovimientoModel(Integer id_movimiento, String fecha_hora, String unidad, Integer posicion,
                           Double medida, String diseno, String no_llanta, String tipo_llanta,
                           Double milimitraje, String causa, String rango_dano, String aplica,
                           String observaciones, String foto, Integer id_usuario, Integer clasificacion, String nombre_clasificacion){
        this.id_movimiento=id_movimiento;
        this.fecha_hora=fecha_hora;
        this.unidad=unidad;
        this.posicion=posicion;
        this.medida=medida;
        this.diseno=diseno;
        this.no_llanta=no_llanta;
        this.tipo_llanta=tipo_llanta;
        this.milimitraje=milimitraje;
        this.causa=causa;
        this.rango_dano=rango_dano;
        this.aplica=aplica;
        this.observaciones=observaciones;
        this.foto=foto;
        this.id_usuario=id_usuario;
        this.clasificacion = clasificacion;
        this.nombre_clasificacion = nombre_clasificacion;
    }

    public Bundle toBundle(){
        Bundle bundle = new Bundle();
        bundle.putInt("id_movimiento", id_movimiento);
        bundle.putString("fecha_hora",fecha_hora);
        bundle.putString("unidad", unidad);
        bundle.putInt("posicion", posicion);
        bundle.putDouble("medida", medida);
        bundle.putString("diseno", diseno);
        bundle.putString("no_llanta", no_llanta);
        bundle.putString("tipo_llanta", tipo_llanta);
        bundle.putString("causa", causa);
        bundle.putString("aplica", aplica);
        bundle.putString("observaciones", observaciones);
        bundle.putString("rango_dano", rango_dano);
        bundle.putDouble("milimitraje", milimitraje);
        bundle.putInt("id_usuario", id_usuario);
        bundle.putString("foto", foto);

        return  bundle;
    }

    public Integer getId(){
        return id_movimiento;
    }

    public String getUnidad(){ return unidad; }

    public String getTitulo(){
        return unidad;
    }

    public String getDescripcion(){
        return  "Pos: " + posicion  + "  No:" +  no_llanta + "  Med:"  + medida + "  Mil:" + milimitraje;
    }
    public String getFecha_hora(){
        return fecha_hora;
    }

    public String getMovimiento(){
        return aplica;
    }

    public String getPosicion(){ return Integer.toString(posicion); }
    public Integer getIntPosicion(){ return posicion;}
    public String getNoLlanta(){ return no_llanta; }

    public Integer getClasificacion(){ return clasificacion;}
    public String getNombreClasificacion() { return nombre_clasificacion; }

}
