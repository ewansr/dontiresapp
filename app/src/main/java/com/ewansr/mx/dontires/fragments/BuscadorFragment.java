package com.ewansr.mx.dontires.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ewansr.mx.dontires.CapturaMovtoActivity;
import com.ewansr.mx.dontires.R;
import com.ewansr.mx.dontires.adapters.Adaptador;
import com.ewansr.mx.dontires.adapters.MovimientosAdapter;
import com.ewansr.mx.dontires.api.Request;
import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.api.mExceptionCode;
import com.ewansr.mx.dontires.models.MovimientoModel;
import com.ewansr.mx.dontires.utilerias.DateU;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import mehdi.sakout.fancybuttons.FancyButton;

public class BuscadorFragment extends Fragment {
    private static Context mContext;
    private ListView listView;
    private EditText cBuscador;
    public View rootView;


    public static BuscadorFragment newInstance(Context context){
        mContext = context;
        BuscadorFragment fragment = new BuscadorFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.activity_buscador, container, false);
            listView = rootView.findViewById(R.id.ListViewMenuBuscador);
            cBuscador = rootView.findViewById(R.id.cBuscador);
            cBuscador.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                        mostrarHistorial(rootView, cBuscador.getText().toString());
                    }
                    return false;
                }
            });


        }
        return rootView;
    }

    public void mostrarHistorial(final View view, String unidad_buscar){
        try{
            @SuppressLint("StaticFieldLeak")
            Request request = new Request(){
                @Override
                public void RequestBeforeExecute(){
                    onBeforeLoad();
                }

                @Override
                public void RequestError(Exception error){
                    onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
                }

                public void RequestCompleted(JSONObject jsonObject){
                    try {
                        if(jsonObject.getInt("stat") == 1){
                            JSONArray array = jsonObject.getJSONArray("data");
                            ArrayList<MovimientoModel> lista = new ArrayList<MovimientoModel>();

                            for ( int i = 0; i < array.length() ; i++) {
                                JSONObject item = array.getJSONObject(i);

                                lista.add(new MovimientoModel(
                                        item.getInt("id_movimiento"),
                                        item.getString("fecha_hora"),
                                        item.getString("unidad"),
                                        item.getInt("posicion"),
                                        item.getDouble("medida"),
                                        item.getString("diseno"),
                                        item.getString("no_llanta"),
                                        item.getString("tipo_llanta"),
                                        item.getDouble("milimitraje"),
                                        item.getString("causa"),
                                        item.getString("rango_dano"),
                                        item.getString("aplica"),
                                        item.getString("observaciones"),
                                        item.getString("foto"),
                                        item.getInt("id_usuario")));
                            }
                            onReady(lista);
                        }else{
                            onError(new mException(mExceptionCode.ITEMS_NOT_FOUND, jsonObject.getString("data")));
                        }
                    } catch (JSONException e) {
                        onError(new mException(mExceptionCode.UNKNOWN, e.getMessage()));
                    }
                }
            };
            JSONObject body = new JSONObject();
            body.put("unit", unidad_buscar);
            String route = "movimientos/unidad/buscar";
            request.post(route, body);
        }catch (Exception e){
            new mException(mExceptionCode.UNKNOWN, e.getMessage());
        }
    }

    public void onBeforeLoad(){    }

    public void onReady(ArrayList<MovimientoModel> mData){
        //if (mData.size() == 0)
        /*
            tvAviso.setVisibility(View.VISIBLE);
        else tvAviso.setVisibility(View.INVISIBLE);
        */

        Adaptador adaptador = new Adaptador((Activity) mContext, mData,1);
        listView.setAdapter(adaptador);
    };
    public void onError(mException error){
        final android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder( mContext );
        builder.setTitle( error.title );
        builder.setMessage( error.message );

        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    };
}
