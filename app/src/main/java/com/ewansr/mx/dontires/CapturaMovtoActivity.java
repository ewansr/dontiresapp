package com.ewansr.mx.dontires;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.api.mExceptionCode;
import com.ewansr.mx.dontires.fragments.MovimientosFragment;
import com.ewansr.mx.dontires.models.MovimientoModel;
import com.ewansr.mx.dontires.request.MovimientosRequest;
import com.ewansr.mx.dontires.utilerias.DateU;

import java.nio.file.FileSystemNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import mehdi.sakout.fancybuttons.FancyButton;

public class CapturaMovtoActivity extends MovimientosRequest {
    private Calendar myCalendar;
    private Context context;
    private ProgressDialog progress;
    private FancyButton cFecha, btnGuardar;
    private Spinner spMontaje, spPosicion, spMedida, spTipoLlanta, spRangoDano;
    private EditText  cDiseno, cMilimetraje, cCausa, cObservaciones;
    private AutoCompleteTextView cNoLlanta, cEconomico;
    private Boolean actualizarDatos = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captura_movto);

        context = this;
        cFecha = findViewById(R.id.cFecha);
        spMontaje = findViewById(R.id.spMontaje);
        spPosicion = findViewById(R.id.spPosicion);
        spMedida = findViewById(R.id.spMedida);
        spTipoLlanta = findViewById(R.id.spTipoLlanta);
        spRangoDano = findViewById(R.id.spRangoDano);
        cDiseno = findViewById(R.id.cDiseno);
        cMilimetraje = findViewById(R.id.cMilimetraje);
        cCausa = findViewById(R.id.cCausa);
        cObservaciones = findViewById(R.id.cObservaciones);
        cNoLlanta = findViewById(R.id.cNoLlanta);
        cEconomico = findViewById(R.id.cEconomico);

        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String[] fechaSeleccionada = {df.format(Calendar.getInstance().getTime())};

        // TODO setear Fecha actual del sistema
        myCalendar = Calendar.getInstance();
        cFecha.setText(DateU.now());

        // TODO: Obtener extras en caso de venir de otra activity con datos precargados
        //TODO cuando se cambie la fecha de trabajo
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                // TODO Auto-generated method stub
                //monthOfYear = monthOfYear + 1;
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                monthOfYear = monthOfYear +1;
                cFecha.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                fechaSeleccionada[0] = year+"-"+monthOfYear+"-"+dayOfMonth;
            }
        };

        cFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        // TODO mostrar u ocultar los controles al usuario
        spMontaje.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO montaje y desmontaje no aplica el rango de daño
                if (spMontaje.getSelectedItem().toString().compareTo("Montaje")==0 || spMontaje.getSelectedItem().toString().compareTo("Desmontaje")==0){
                    spRangoDano.setVisibility(View.GONE);
                    cMilimetraje.setVisibility(View.VISIBLE);
                }else{
                    spRangoDano.setVisibility(View.VISIBLE);
                    cMilimetraje.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Bundle bundle = getIntent().getExtras();
        if(bundle!=null && !bundle.isEmpty()){
            actualizarDatos = true;
            fechaSeleccionada[0] = bundle.getString("fecha_hora");
            cFecha.setText(bundle.getString("fecha_hora"));
            cEconomico.setText(bundle.getString("unidad"));
            selectValue(spPosicion,bundle.getInt("posicion"));
            selectValue(spMedida, bundle.getDouble("medida"));
            cDiseno.setText(bundle.getString("diseno"));
            cNoLlanta.setText(bundle.getString("no_llanta"));
            selectValue(spTipoLlanta,bundle.getString("tipo_llanta"));
            cCausa.setText(bundle.getString("causa"));
            selectValue(spMontaje, bundle.getString("aplica"));
            cObservaciones.setText(bundle.getString("observaciones"));
            selectValue(spRangoDano, bundle.getString("rango_dano"));
            cMilimetraje.setText(Double.toString(bundle.getDouble("milimitraje")));
        }

        // TODO guardar los datos
        btnGuardar = findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bundle mbundle = new Bundle();
                if(actualizarDatos)
                    {mbundle.putString("id_movimiento", Integer.toString(bundle.getInt("id_movimiento")));}
                else
                    {mbundle.putString("id_movimiento", "0");}

                mbundle.putString("fecha_hora",fechaSeleccionada[0]);
                mbundle.putString("unidad", cEconomico.getText().toString());
                mbundle.putString("posicion", spPosicion.getSelectedItem().toString());
                mbundle.putString("medida", spMedida.getSelectedItem().toString());
                mbundle.putString("diseno", cDiseno.getText().toString());
                mbundle.putString("no_llanta", cNoLlanta.getText().toString());
                mbundle.putString("tipo_llanta", spTipoLlanta.getSelectedItem().toString());
                mbundle.putString("causa", cCausa.getText().toString());
                mbundle.putString("aplica", spMontaje.getSelectedItem().toString());
                mbundle.putString("observaciones", cObservaciones.getText().toString());
                mbundle.putString("rango_dano", spRangoDano.getSelectedItem().toString());
                mbundle.putString("milimitraje", cMilimetraje.getText().toString());
                mbundle.putString("id_usuario", "1");
                if (validarCampos()){
                        guardar(mbundle, actualizarDatos); // TODO: el parámetro actualiza datos solo indica si es un insert o un update
                }
            }
        });

        // TODO validar el registro de los campos
    }

    private void selectValue(Spinner spinner, String value) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().compareTo(value)==0) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    private void selectValue(Spinner spinner, Double value) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (Double.parseDouble(spinner.getItemAtPosition(i).toString()) == value) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    private void selectValue(Spinner spinner, Integer value) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (Integer.parseInt(spinner.getItemAtPosition(i).toString()) == value) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public Boolean validarCampos(){
        Boolean pasa = true;

        if (cDiseno.getText().toString().isEmpty()) {
            pasa = false;
            onError(new mException(mExceptionCode.ITEMS_EMPTY, "Diseño no puede estar vacio."));
            //System.exit(0);
        }
        if (cNoLlanta.getText().toString().isEmpty()){
            pasa=false;
            onError(new mException(mExceptionCode.ITEMS_EMPTY, "No. de llanta no puede estar vacío."));
            //System.exit(0);
        }
        if (cEconomico.getText().toString().isEmpty()){
            pasa=false;
            onError(new mException(mExceptionCode.ITEMS_EMPTY, "No. de económico no puede estar vacío."));
            //System.exit(0);
        }

        return pasa;
    }

    @Override
    public void onBeforeLoad() {
        progress = ProgressDialog.show(this, "Espere...",
                "Enviando datos al servidor...", false);
        Toast.makeText(context, "Enviado datos al servidor...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReady(ArrayList<MovimientoModel> mData) {
        progress.dismiss();
        final android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder( context );
        builder.setTitle( "Aviso" );
        builder.setMessage( "Datos guardador correctamente." );

        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        }).show();
        finish();

    }

    @Override
    public void onError(mException error) {
        progress.dismiss();
        final android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder( context );
        builder.setTitle( error.title );
        builder.setMessage( error.message );

        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        }).show();
    }
}
