package com.ewansr.mx.dontires.models;

/**
 * Created by Saul on 26/02/2018.
 */

public class LoginModel {
    public Integer id;
    public String usuario;
    public String contrasena;
    public Integer activo;


    public LoginModel(Integer id, String usuario, String contrasena, Integer activo){
        this.id=id;
        this.usuario=usuario;
        this.contrasena=contrasena;
        this.activo=activo;
    }
}
