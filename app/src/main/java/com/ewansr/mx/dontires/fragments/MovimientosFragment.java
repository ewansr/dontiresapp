package com.ewansr.mx.dontires.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.ewansr.mx.dontires.CapturaMovtoActivity;
import com.ewansr.mx.dontires.R;
import com.ewansr.mx.dontires.adapters.MovimientosAdapter;
import com.ewansr.mx.dontires.api.Request;
import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.api.mExceptionCode;
import com.ewansr.mx.dontires.models.MovimientoModel;
import com.ewansr.mx.dontires.utilerias.DateU;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import mehdi.sakout.fancybuttons.FancyButton;

public class MovimientosFragment extends Fragment {
    private static Context mContext;
    private RecyclerView listView;
    private FancyButton btnFecha, btnActualizar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private static StaggeredGridLayoutManager layoutManager;
    private String fechaSeleccionada;
    private TextView tvAviso;
    private FloatingActionButton fab;

    public View rootView;


    public static MovimientosFragment newInstance(Context context){
        mContext = context;
        MovimientosFragment fragment = new MovimientosFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Calendar myCalendar;
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.movimientos_activity, container, false);
            listView = rootView.findViewById(R.id.ListViewMenu);
            btnFecha = rootView.findViewById(R.id.fechaMovimientos);
            btnActualizar = rootView.findViewById(R.id.btnActualizar);
            tvAviso = rootView.findViewById(R.id.tvDatos);
            fab = rootView.findViewById(R.id.fabAdd);
            myCalendar = Calendar.getInstance();

            btnFecha.setText(DateU.now());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            fechaSeleccionada = df.format(Calendar.getInstance().getTime());

            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                // TODO Auto-generated method stub

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                monthOfYear = monthOfYear +1;
                btnFecha.setText(dayOfMonth+"/"+monthOfYear+"/"+year);
                fechaSeleccionada = year+"-"+monthOfYear+"-"+dayOfMonth;
                cargaElementos(rootView, fechaSeleccionada);
            }};

            btnFecha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(mContext, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            btnActualizar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargaElementos(rootView, fechaSeleccionada);
                }
            });

            cargaElementos(rootView, this.fechaSeleccionada);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, CapturaMovtoActivity.class);
                    startActivityForResult(i,2);
                }
            });
            /*
            swipeRefreshLayout = rootView.findViewById(R.id.swipeRefresh);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    cargaElementos(rootView);
                }
            });*/
        }
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        cargaElementos(rootView,fechaSeleccionada);

    }



    public void cargaElementos(final View view, String fechaSeleccionada){
        try{
            @SuppressLint("StaticFieldLeak")
            Request request = new Request(){
                @Override
                public void RequestBeforeExecute(){
                    onBeforeLoad();
                }

                @Override
                public void RequestError(Exception error){
                    onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
                }

                public void RequestCompleted(JSONObject jsonObject){
                    try {
                        if(jsonObject.getInt("stat") == 1){
                            JSONArray array = jsonObject.getJSONArray("data");
                            ArrayList<MovimientoModel> lista = new ArrayList<MovimientoModel>();

                            for ( int i = 0; i < array.length() ; i++) {
                                JSONObject item = array.getJSONObject(i);

                                lista.add(new MovimientoModel(
                                        item.getInt("id_movimiento"),
                                        item.getString("fecha_hora"),
                                        item.getString("unidad"),
                                        item.getInt("posicion"),
                                        item.getDouble("medida"),
                                        item.getString("diseno"),
                                        item.getString("no_llanta"),
                                        item.getString("tipo_llanta"),
                                        item.getDouble("milimitraje"),
                                        item.getString("causa"),
                                        item.getString("rango_dano"),
                                        item.getString("aplica"),
                                        item.getString("observaciones"),
                                        item.getString("foto"),
                                        item.getInt("id_usuario")));
                            }
                            onReady(lista);
                        }else{
                            onError(new mException(mExceptionCode.ITEMS_NOT_FOUND, jsonObject.getString("data")));
                        }
                    } catch (JSONException e) {
                        onError(new mException(mExceptionCode.UNKNOWN, e.getMessage()));
                    }
                }
            };

            String route = "movimientos/datos/" + fechaSeleccionada;
            request.get(route);
        }catch (Exception e){
            new mException(mExceptionCode.UNKNOWN, e.getMessage());
        }
    }

    public void onBeforeLoad(){    }

    public void onReady(ArrayList<MovimientoModel> mData){
        if (mData.size() == 0)
            tvAviso.setVisibility(View.VISIBLE);
        else tvAviso.setVisibility(View.INVISIBLE);

        layoutManager = new StaggeredGridLayoutManager( 1, 1 );
        listView.setLayoutManager( layoutManager );
        MovimientosAdapter adaptador = new MovimientosAdapter(mData,mContext);
        listView.setAdapter(adaptador);
    };
    public void onError(mException error){
        final android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder( mContext );
        builder.setTitle( error.title );
        builder.setMessage( error.message );

        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    };
}
