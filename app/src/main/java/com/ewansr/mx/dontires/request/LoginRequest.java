package com.ewansr.mx.dontires.request;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;

import com.ewansr.mx.dontires.api.Request;
import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.api.mExceptionCode;
import com.ewansr.mx.dontires.models.LoginModel;

import org.json.JSONObject;

public abstract class LoginRequest extends AppCompatActivity {
    public void login(String usuario, String contrasena){
        try{
            @SuppressLint("StaticFieldLeak")
                Request request = new Request(){
                @Override
                public void RequestBeforeExecute(){
                  onBeforeLoad();
                }

                @Override
                public void RequestError(Exception error) {
                  onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
                }

                @Override
                public void RequestCompleted(JSONObject jsonObject) {
                    try {
                        // @todo: Si todo sale como debe ser
                        if (jsonObject.getInt("stat") == 1) {
                            JSONObject joLocal = jsonObject.getJSONObject("data");
                            onReady(new LoginModel(
                                    joLocal.optInt("id", -9),
                                    joLocal.getString("usuario"),
                                    joLocal.getString("contrasena"),
                                    joLocal.getInt("activo")
                            ));
                        }// todo:  !! ups !! algo salió mal
                        else {
                            onError(new mException(mExceptionCode.ITEMS_NOT_FOUND, jsonObject.getString("data").toString()));
                        }
                    } catch (Exception error) {
                        new mException(mExceptionCode.UNKNOWN, error.getMessage());
                    }
                }
            };

            // Todo: Llamado a la función

            // JSONObject body = new JSONObject();
            String route = "login/";
            request.get( route + usuario + ',' + contrasena );
        }catch (Exception e){
            new mException(mExceptionCode.UNKNOWN, e.getMessage());
        }
    }

    public abstract void onBeforeLoad();
    public abstract void onReady(LoginModel mUsuario);
    public abstract void onError(mException error);
}
