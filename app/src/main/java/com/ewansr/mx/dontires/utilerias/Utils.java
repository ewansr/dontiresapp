package com.ewansr.mx.dontires.utilerias;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ImageView;

import com.ewansr.mx.dontires.SQL.SQLiteDBConfig;
import com.ewansr.mx.dontires.SQL.SQLiteDBDataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by EwanS on 17/08/2016.
 */
public class Utils  {

    /*
     *@Context Parametro contexto de activity
     */

    public static String formatDate = "dd/MM/yyyy";
    public static String ruta_absoluta_archivo;
    private static ProgressDialog progressDialog;


    public static boolean clonar_imagen(File origen, String nombre_archivo, Context context) {
        Boolean exitoso = false;
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);

        if (res == PackageManager.PERMISSION_GRANTED) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(origen.getAbsolutePath(), options);
            exitoso = storeImage(bitmap, context, nombre_archivo);
        }
        return exitoso;
    }

    private static Boolean storeImage(Bitmap image, Context context, String nombre_archivo ) {
        File pictureFile = getOutputMediaFile(context, nombre_archivo);
        Boolean estatus = false;
        if (pictureFile == null) {
            estatus = false;
            Log.d("Error imagen",
                    "Error al crear el archivo, revisa los permisos de almacenamiento: ");// e.getMessage());
        }else {
            try {
                ruta_absoluta_archivo = null;
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                fos.close();
                ruta_absoluta_archivo = pictureFile.getAbsolutePath();
                estatus = true;
            } catch (FileNotFoundException e) {
                estatus = false;
                Log.d("Error imagen", "Archivo no encontrado: " + e.getMessage());
            } catch (IOException e) {
                estatus = false;
                Log.d("Error imagen", "Error al acceder al archivo " + e.getMessage());
            }
        }
        return estatus;
    }

    private static File getOutputMediaFile(Context context, String nombre_archivo){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + context.getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
//        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName=nombre_archivo;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static Boolean isEquals(String string, String stringCompare){
        return new String(string).equals(stringCompare);
    }

    public static void showMessage(String title, String message, Context context, AlertDialog.Builder builder ){
            builder = new AlertDialog.Builder( context );
            builder
                    .setTitle( title )
                    .setMessage( message )
                    .create()
                    .show();
    }

    public static void showMessage(String title, String message, Context context ){
        AlertDialog.Builder builder = new AlertDialog.Builder( context );
        builder
                .setTitle( title )
                .setMessage( message )
                .create()
                .show();
    }

    public static android.app.AlertDialog crear_alerta(Context context, String titulo, String mensaje ) {
        final android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder( context );
        builder.setTitle( titulo );
        builder.setMessage( mensaje );

        builder.setPositiveButton( "Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }

    public static void showLoadMessage(String message, ProgressDialog progressDialog, Context context ){
        progressDialog = new ProgressDialog( context );
        progressDialog.setMessage( message );
        progressDialog.show();
    }

    public static void showLoadMessage(String message, ProgressDialog progressDialog ){
        progressDialog.setMessage( message );
        progressDialog.show();
    }


    public static class LoadRemoteImg extends AsyncTask<String, Void, Bitmap> {
        private Exception exception;
        private ImageView img;

        public LoadRemoteImg(ImageView img){
            this.img = img;
        }

        protected Bitmap doInBackground(String... params) {
            try {

                URL url = null;
                try {
                    url = new URL(params[0].toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                Bitmap bmp = null;
                try {
                    bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return bmp;
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }

        protected void onPostExecute(Bitmap bmp) {
            if (bmp != null && img != null) {
                img.setImageBitmap(bmp);
            }
        }
    }

    public static JSONArray keyJson(JSONArray array, String key) throws JSONException {
        JSONArray value = null;
        for (int i = 0; i < array.length(); i++)
        {
            JSONObject item = array.getJSONObject(i);
            if (item.has(key)){
                value = item.getJSONArray(key);
                break;
            }
        }

        return value;
    }

    public static JSONArray keyJson(JSONObject array, String key)  {
        JSONArray value = null;
        JSONArray item = null;
        try {
            item = array.getJSONArray(key);
            value = item;
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return value;
    }

    public static int inc(int i){
        i = i + 1;
        return i;
    }


    /*
      * Devuelve una fecha formateada MX
     */
    public static String todayIs(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatDate);
        return simpleDateFormat.format(date);
    }

    public static String incDay(String sDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatDate);
        Calendar c = Calendar.getInstance();
        try {
            Date date = simpleDateFormat.parse(sDate);
            c.setTime(simpleDateFormat.parse(sDate));
            c.add(Calendar.DATE, 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(c.getTime());
    }

    public static String decDay(String sDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatDate);
        Calendar c = Calendar.getInstance();
        try {
            Date date = simpleDateFormat.parse(sDate);
            c.setTime(simpleDateFormat.parse(sDate));
            c.add(Calendar.DATE, -1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(c.getTime());
    }

    public static String getTokenId(Context context){
        SQLiteDBDataSource dataSource = new SQLiteDBDataSource(context);
        Cursor c = dataSource.selectValue(context, SQLiteDBConfig.TABLA_USUARIO);
        String token_temporal = null;
        while(c.moveToNext()) {
            token_temporal = c.getString(c.getColumnIndex(SQLiteDBConfig.cols_user.token_firebase));
        }
        return token_temporal;
    }

    public static Bundle getUserData(Context context){
        SQLiteDBDataSource dataSource = new SQLiteDBDataSource(context);
        Cursor c = dataSource.selectValue(context, SQLiteDBConfig.TABLA_USUARIO);
        Bundle bundle = new Bundle();
        while(c.moveToNext()){
            bundle.putString("user", c.getString(c.getColumnIndex(SQLiteDBConfig.cols_user.usuario)));
            bundle.putString("pass", c.getString(c.getColumnIndex(SQLiteDBConfig.cols_user.contrasena)));
        }

        return bundle;
    }

    /* el que lo lea*/
    public static String asDateParam(Date date){
        String dateFormatted = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        dateFormatted = simpleDateFormat.format(date);
        return dateFormatted;
    }

    public static Date stringToDate(String strDate)  {
        Date dateFormatted = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            dateFormatted = simpleDateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatted;
    }

    public static void showDialog(Context mContext, ProgressDialog progressDialog){
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setTitle("Aviso");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            public void run() {
            }
        }).start();
    }

}

