package com.ewansr.mx.dontires.models;

public class ModelCategorias {
    Integer id_categoria;
    String categoria;
    public ModelCategorias(Integer id_categoria, String categoria){
        this.id_categoria = id_categoria;
        this.categoria = categoria;
    }

    public Integer getId(){
        return  this.id_categoria;
    }

    public String getTitulo(){
        return this.categoria;
    }
}
