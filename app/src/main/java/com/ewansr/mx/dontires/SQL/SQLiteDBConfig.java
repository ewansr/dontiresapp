package com.ewansr.mx.dontires.SQL;

/**
 * Created by Saulo on Mar 2018.
 */
public class SQLiteDBConfig {
    public static String DATABASE_NAME = "database_local.db";
    public static int DATABASE_VERSION = 2;
    public static final String TABLA_USUARIO = "main_usuario";
    public static final String STRING_TYPE    = "text";
    public static final String INT_TYPE       = "integer";

    public static class cols_user {
        public static final String id = "id";
        public static final String usuario = "usuario";
        public static final String email = "email";
        public static final String created_at = "created_at";
        public static final String updated_at = "updated_at";
        public static final String nombres = "nombres";
        public static final String apellidos = "apellidos";
        public static final String telefono = "telefono";
        public static final String tipo_usuario = "tipo_usuario";
        public static final String estatus = "estatus";
        public static final String id_estado = "id_estado";
        public static final String id_ciudad = "id_ciudad";
        public static final String calle = "calle";
        public static final String colonia = "colonia";
        public static final String codigo_postal = "codigo_postal";
        public static final String num_interior = "num_interior";
        public static final String num_exterior = "num_exterior";
        public static final String id_firebase = "id_firebase";
        public static final String total_sesiones = "total_sesiones";
        public static final String token_firebase = "token_firebase";
        public static final String contrasena = "contrasena";

    }

    public static final String CREATE_USER =
            "create table "+ TABLA_USUARIO + "(" +
                    cols_user.id  + " " + INT_TYPE + ","+
                    cols_user.usuario  + " " + STRING_TYPE + " null,"+
                    cols_user.contrasena  + " " + STRING_TYPE + " null,"+
                    cols_user.email  + " " + STRING_TYPE + " null,"+
                    cols_user.created_at  + " " + STRING_TYPE + " null,"+
                    cols_user.updated_at  + " " + STRING_TYPE + " null,"+
                    cols_user.nombres  + " " + STRING_TYPE + " null,"+
                    cols_user.apellidos  + " " + STRING_TYPE + " null,"+
                    cols_user.telefono  + " " + STRING_TYPE + " null,"+
                    cols_user.tipo_usuario  + " " + STRING_TYPE + " null,"+
                    cols_user.estatus  + " " + STRING_TYPE + " null,"+
                    cols_user.id_estado  + " " + INT_TYPE + " null,"+
                    cols_user.id_ciudad  + " " + INT_TYPE + " null,"+
                    cols_user.calle  + " " + STRING_TYPE + " null,"+
                    cols_user.colonia  + " " + STRING_TYPE + " null,"+
                    cols_user.codigo_postal  + " " + STRING_TYPE + " null,"+
                    cols_user.num_interior  + " " + STRING_TYPE + " null,"+
                    cols_user.num_exterior  + " " + STRING_TYPE + " null,"+
                    cols_user.id_firebase  + " " + STRING_TYPE + " null,"+
                    cols_user.total_sesiones  + " " + INT_TYPE + " ," +
                    cols_user.token_firebase + " " + STRING_TYPE + " null)";


    public static String user_seeder = "INSERT INTO " + TABLA_USUARIO + " VALUES( -1, '', '', '', '', '', " +
                                       "'', '', '', '', '', -1, -1, '', '', '', '', '', '', -1, '' )";

}
