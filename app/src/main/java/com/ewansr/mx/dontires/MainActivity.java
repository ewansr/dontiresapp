package com.ewansr.mx.dontires;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.ewansr.mx.dontires.fragments.BuscadorFragment;
import com.ewansr.mx.dontires.fragments.MovimientosFragment;
import com.ewansr.mx.dontires.fragments.UltimosMovimientosFragment;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private MovimientosFragment movimientosFragment;
    private UltimosMovimientosFragment ultimosMovimientosFragment;
    private BuscadorFragment buscadorFragment;
    private static Context context;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    getSupportFragmentManager().beginTransaction().replace(R.id.container,  movimientosFragment).commit();
                    return true;
                case R.id.navigation_dashboard:

                    getSupportFragmentManager().beginTransaction().replace(R.id.container, ultimosMovimientosFragment).commit();
                    return true;
                case R.id.navigation_notifications:

                    getSupportFragmentManager().beginTransaction().replace(R.id.container, buscadorFragment).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);




        // TODO inicializar el primer fragment del usuario

        movimientosFragment = MovimientosFragment.newInstance(context);
        ultimosMovimientosFragment = UltimosMovimientosFragment.newInstance(context);
        buscadorFragment = BuscadorFragment.newInstance(context);

        getSupportFragmentManager().beginTransaction().replace(R.id.container,  movimientosFragment).commit();
    }

}
