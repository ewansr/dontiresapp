package com.ewansr.mx.dontires.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ewansr.mx.dontires.MovimientosUnidadActivity;
import com.ewansr.mx.dontires.R;
import com.ewansr.mx.dontires.models.MovimientoModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Saul on 06/11/2017.
 */

public class Adaptador extends BaseAdapter {
    private Activity activity; //Activity desde el cual se hace referencia al llenado de la lista
    private ArrayList<MovimientoModel> arrayItems; // Lista de items
    public Integer layoutBase;
    // Constructor con parámetros que recibe la Acvity y los datos de los items.
    public Adaptador(Activity activity, ArrayList<MovimientoModel> listaItems, Integer layoutBase){
        super();
        this.activity = activity;
        this.arrayItems = new ArrayList<MovimientoModel>(listaItems);
        this.layoutBase = layoutBase;
    }

    // Retorna el número de items de la lista
    @Override
    public int getCount() {
        return arrayItems.size();
    }
    // Retorna el objeto TitularItems de la lista
    @Override
    public Object getItem(int position) {
        return arrayItems.get(position);
    }
    // Retorna la posición del item en la lista
    @Override
    public long getItemId(int position) {
        return position;
    }

    // Todo Clase estática que contiene los elementos de la lista

    public static class Fila
    {
        TextView txtTitle;
        TextView txtDescription;
        ImageView img;
        FloatingActionButton fabDetalle;

    }
    // TODO Método que retorna la vista formateada
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Fila view = new Fila();
        LayoutInflater inflator = activity.getLayoutInflater();
        final MovimientoModel itm = arrayItems.get(position);

         // TODO Condicional para recrear la vista y no distorcionar el número de elementos
        if(convertView==null)
        {
            if (layoutBase == 0) {
                convertView = inflator.inflate(R.layout.list_base, parent, false);
            }else{
                convertView = inflator.inflate(R.layout.list_base_ii, parent, false);
            }

            view.txtTitle =  convertView.findViewById(R.id.txtTitle);
            view.txtDescription =  convertView.findViewById(R.id.txtDescription);
            view.img = convertView.findViewById(R.id.imgItem);
            view.fabDetalle = convertView.findViewById(R.id.fabDetalle);

            final Fila finalView = view;
            view.fabDetalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(activity, MovimientosUnidadActivity.class);
                    //Bundle bundle = new Bundle();
                    //bundle.putSerializable("datos", );
                    //i.putExtras(bundle);
                    i.putExtra("unidad", finalView.txtTitle.getText().toString());
                    activity.startActivity(i);
                }
            });

            convertView.setTag(view);
        }
        else
        {
            view = (Fila)convertView.getTag();
        }
        // TODO Se asigna el dato proveniente del objeto
        if (layoutBase==0) {
            view.txtTitle.setText(itm.getTitulo());
        }else{
            view.txtTitle.setText(itm.getTitulo()+'@'+itm.getFecha_hora().substring(0,itm.getFecha_hora().indexOf(' ')));
        }
        view.txtDescription.setText(itm.getDescripcion());

        if (itm.getMovimiento().compareTo("Desmontaje")==0) {
            view.img.setImageResource(R.drawable.desmontaje_256);
        }
        if (itm.getMovimiento().compareTo("Reparacion")==0) {
            view.img.setImageResource(R.drawable.reparacion_256);
        }
        if (itm.getMovimiento().compareTo("Montaje")==0) {
            view.img.setImageResource(R.drawable.montaje_256);
        }
        if (itm.getMovimiento().compareTo("Volada")==0) {
            view.img.setImageResource(R.drawable.volada_256);
        }

        if (itm.getClasificacion() != -1 ){
            switch (itm.getClasificacion()){
                case 1:
                    view.img.setImageResource(R.drawable.truck_icon);
                    break;
                case 2:
                    view.img.setImageResource(R.drawable.trailer_icon);
                    break;
                case 3:
                    view.img.setImageResource(R.drawable.dollie);
                    break;
            }
        }

        return convertView;
    }
}