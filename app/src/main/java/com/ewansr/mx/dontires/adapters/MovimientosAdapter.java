package com.ewansr.mx.dontires.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ewansr.mx.dontires.CapturaMovtoActivity;
import com.ewansr.mx.dontires.R;
import com.ewansr.mx.dontires.api.Request;
import com.ewansr.mx.dontires.models.MovimientoModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mehdi.sakout.fancybuttons.FancyButton;

public class MovimientosAdapter extends RecyclerView.Adapter<MovimientosAdapter.movimientosViewHolder>{
    private ArrayList<MovimientoModel> items;
    private Context mContext;

    // Contructor
    public MovimientosAdapter(ArrayList<MovimientoModel> items, Context mContext) {
        this.items = items;
        this.mContext = mContext;
    }

    @Override
    public MovimientosAdapter.movimientosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.cv_template, parent, false);
        movimientosViewHolder viewHolder = new movimientosViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull movimientosViewHolder holder, final int position) {
        holder.title.setText(items.get(position).getTitulo());
        holder.subtitle.setText(items.get(position).getDescripcion());
        holder.tvTipoMovimiento.setText(items.get(position).getMovimiento());

        holder.btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(mContext)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Eliminar")
                        .setMessage("¿Estás seguro que deseas eliminar el movimiento?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteMovto(items.get(position).getId(), position);
                                items.remove(position);
                                notifyItemRemoved(position);
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

        holder.btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, CapturaMovtoActivity.class);
                Bundle bundle = items.get(position).toBundle();
                i.putExtras(bundle);
                mContext.startActivity(i);
            }
        });

        if (items.get(position).getMovimiento().compareTo("Desmontaje")==0) {
            //holder.img.setImageResource(R.drawable.desmontaje_256);
            holder.tvTipoMovimiento.setTextColor(mContext.getResources().getColor(R.color.color_desmontaje));
            holder.btnMovimiento.setBackgroundResource(R.color.color_desmontaje);
        }
        if (items.get(position).getMovimiento().compareTo("Reparacion")==0) {
            holder.tvTipoMovimiento.setTextColor(mContext.getResources().getColor(R.color.color_renovado));
            holder.btnMovimiento.setBackgroundResource(R.color.color_renovado);
        }
        if (items.get(position).getMovimiento().compareTo("Montaje")==0) {
            holder.tvTipoMovimiento.setTextColor(mContext.getResources().getColor(R.color.color_montaje));
            holder.btnMovimiento.setBackgroundResource(R.color.color_montaje);
        }
        if (items.get(position).getMovimiento().compareTo("Volada")==0) {
            holder.tvTipoMovimiento.setTextColor(mContext.getResources().getColor(R.color.color_volada));
            holder.btnMovimiento.setBackgroundResource(R.color.color_volada);
        }


        /*
        String url_imagen = items.get(position).foto_especialista;
        if ((url_imagen != null)) {
            Picasso.with(mContext)
                    .load(url_imagen)
                    .noFade()
                    .into(holder.profile);
        }*/
    }

    public void deleteMovto(Integer idMovimiento, final Integer pos) {
        try{
            @SuppressLint("StaticFieldLeak")
            Request request = new Request(){
                @Override
                public void RequestBeforeExecute(){}

                @Override
                public void RequestError(Exception error){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void RequestCompleted(JSONObject jsonObject){
                    try {
                        if (jsonObject.getInt("stat") == 1) {
                            Toast.makeText(mContext, jsonObject.getString("data"), Toast.LENGTH_LONG).show();
                        }
                    }catch (JSONException error){
                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            };

        String route = "movimientos/borrar/" + idMovimiento;
        request.delete(route);

        }catch (Exception e){
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public Boolean retorno(Boolean valor){
        return valor;
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView ){
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class movimientosViewHolder extends RecyclerView.ViewHolder{
        TextView title, subtitle;
        FancyButton btnMovimiento, btnEliminar, btnEditar;
        TextView tvTipoMovimiento;
        movimientosViewHolder(View view){
            super(view);
            title = view.findViewById(R.id.title);
            subtitle = view.findViewById(R.id.subtitle);
            btnMovimiento = view.findViewById(R.id.btnTipoMovimiento);
            tvTipoMovimiento = view.findViewById(R.id.tvTipoMovimiento);
            btnEliminar = view.findViewById(R.id.btnEliminar);
            btnEditar = view.findViewById(R.id.btnEditar);
        }
    }

}
