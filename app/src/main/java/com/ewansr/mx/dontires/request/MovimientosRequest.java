package com.ewansr.mx.dontires.request;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ewansr.mx.dontires.api.Request;
import com.ewansr.mx.dontires.api.mException;
import com.ewansr.mx.dontires.api.mExceptionCode;
import com.ewansr.mx.dontires.models.MovimientoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public abstract class MovimientosRequest extends AppCompatActivity{

    public void guardar(Bundle bundle, Boolean actualizar){
        try{
            @SuppressLint("StaticFieldLeak")
            Request request = new Request(){
                @Override
                public void RequestBeforeExecute(){ onBeforeLoad(); }

                @Override
                public void RequestCompleted(JSONObject jsonObject) throws JSONException {
                    try {
                        // @todo: Si todo sale como debe ser
                        if (jsonObject.getInt("stat") == 1) {
                            JSONObject joLocal = jsonObject.getJSONObject("data");
                            ArrayList<MovimientoModel> lista = new ArrayList<MovimientoModel>();
                            lista.add(new MovimientoModel(
                                    joLocal.getInt("id_movimiento"),
                                    joLocal.getString("fecha_hora"),
                                    joLocal.getString("unidad"),
                                    joLocal.getInt("posicion"),
                                    joLocal.getDouble("medida"),
                                    joLocal.getString("diseno"),
                                    joLocal.getString("no_llanta"),
                                    joLocal.getString("tipo_llanta"),
                                    joLocal.getDouble("milimitraje"),
                                    joLocal.getString("causa"),
                                    joLocal.getString("rango_dano"),
                                    joLocal.getString("aplica"),
                                    joLocal.getString("observaciones"),
                                    "sinfoto",
                                    joLocal.getInt("id_usuario")
                            ));
                            onReady(lista);
                        }// todo:  !! ups !! algo salió mal
                        else {
                            onError(new mException(mExceptionCode.ITEMS_NOT_FOUND, jsonObject.getString("data").toString()));
                        }
                    } catch (Exception error) {
                        onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
                    }

                }

                @Override
                public void RequestError(Exception error) {
                    onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
                }
            };

            String route;
            if (!actualizar){ route = "movimientos/registro";}
            else{ route = "movimientos/actualiza/"+bundle.getString("id_movimiento");}
            JSONObject body = new JSONObject();

            // TODO montaje y desmontaje no aplica el rango de daño
            // TODO reparación y volada no aplica milimetraje y foto si

            String rangoDano = bundle.getString("rango_dano");
            String milimetraje = bundle.getString("milimitraje");

            if (bundle.getString("aplica").compareTo("Montaje")==0 || bundle.getString("aplica").compareTo("Montaje")==0){
                rangoDano = "NA";
            }

            if (bundle.getString("aplica").compareTo("Renovada")==0 || bundle.getString("aplica").compareTo("Volada")==0){
                milimetraje = "0";
            }

            if (milimetraje.isEmpty()){milimetraje ="0";}

            body.put("id_movimiento", bundle.getString("id_movimiento"));
            body.put("fecha_hora", bundle.getString("fecha_hora"));
            body.put("unidad", bundle.getString("unidad"));
            body.put("posicion", bundle.getString("posicion"));
            body.put("medida", bundle.getString("medida"));
            body.put("diseno", bundle.getString("diseno"));
            body.put("no_llanta", bundle.getString("no_llanta"));
            body.put("tipo_llanta", bundle.getString("tipo_llanta"));
            body.put("milimitraje", milimetraje);
            body.put("causa", bundle.getString("causa"));
            body.put("rango_dano", rangoDano);
            body.put("aplica", bundle.getString("aplica"));
            body.put("observaciones", bundle.getString("observaciones"));
            body.put("id_usuario", bundle.getString("id_usuario"));
            request.post(route, body);

        }catch (Exception e){
            new mException(mExceptionCode.UNKNOWN, e.getMessage());
        }
    }

    public void consultaUltimosMontajes(String noEconomico) {
        try {
            @SuppressLint("StaticFieldLeak")
            Request request = new Request() {
                @Override
                public void RequestBeforeExecute() { onBeforeLoad(); }

                @Override
                public void RequestCompleted(JSONObject jsonObject) throws JSONException{
                    // TODO: buscando el bendito 1 para saber que todo va viento en popa Jeje
                    if (jsonObject.getInt("stat") == 1){
                        JSONArray array = jsonObject.getJSONArray("data");
                        ArrayList<MovimientoModel> lista = new ArrayList<MovimientoModel>();
                        for(int i = 0; i<array.length();i++){
                            JSONObject item = array.getJSONObject(i);
                            lista.add(new MovimientoModel(item.getInt("id_movimiento"),
                                    item.getString("fecha_hora"),
                                    item.getString("unidad"),
                                    item.getInt("posicion"),
                                    item.getDouble("medida"),
                                    item.getString("diseno"),
                                    item.getString("no_llanta"),
                                    item.getString("tipo_llanta"),
                                    item.getDouble("milimitraje"),
                                    item.getString("causa"),
                                    item.getString("rango_dano"),
                                    item.getString("aplica"),
                                    item.getString("observaciones"),
                                    item.getString("foto"),
                                    item.getInt("id_usuario")));
                        }
                        onReady(lista);
                    }else{ // TODO Se va al chorizo todo por que hay un error y hay que hacerselo saber al usuario
                        onError(new mException(mExceptionCode.ITEMS_NOT_FOUND, jsonObject.getString("data").toString()));
                    }
                }

                @Override
                public void RequestError(Exception error) {
                    onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
                }
            };
            JSONObject body = new JSONObject();
            body.put("unit", noEconomico);
            String route = "movimientos/unidad/montajes";
            request.post(route, body);
        }catch (Exception error){
            onError(new mException(mExceptionCode.UNKNOWN, error.getMessage()));
        }
    }


    public abstract void onBeforeLoad();
    public abstract void onReady(ArrayList<MovimientoModel> mData);
    public abstract void onError(mException error);
}
